## 作者:

- 康林 <kl222@126.com>

## 贡献者：

- 缪正伟 <393758926@qq.com>
- 所有贡献者：[https://github.com/KangLin/RabbitIm/graphs/contributors](https://github.com/KangLin/RabbitIm/graphs/contributors)

## 工具

- [Qt (LGPL v2.1)](http://qt.io/)
- [x] c compiler
  + GCC/G++
  + MSVC
- [cmake](https://cmake.org/)
- Git: [https://www.git-scm.com](https://www.git-scm.com/)

## 第三方库：

- [x] [必选] [RabbitCommon](https://github.com/KangLin/RabbitCommon)
- [x] [可选] [QXmpp](https://github.com/qxmpp-project/qxmpp)
- [x] [可选] [QZXing](https://github.com/ftylitak/qzxing)
