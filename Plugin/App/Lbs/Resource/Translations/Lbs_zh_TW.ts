<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_TW">
<context>
    <name>CFrmLbsMain</name>
    <message>
        <source>Motion</source>
        <translation>運動</translation>
    </message>
    <message>
        <source>Track</source>
        <translation>跟蹤</translation>
    </message>
</context>
<context>
    <name>CLbsMotion</name>
    <message>
        <source>Motion</source>
        <translation>運動</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>開始</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>暫停</translation>
    </message>
    <message>
        <source>%1
Distance(km)</source>
        <translation>%1
距離(公里)</translation>
    </message>
    <message>
        <source>%1
Duration</source>
        <translation>%1
距離</translation>
    </message>
    <message>
        <source>%1
Accuracy(m)</source>
        <translation>%1
精度</translation>
    </message>
    <message>
        <source>%1
Speed(km/h)</source>
        <translation type="unfinished">%1
速度（公里/小時)</translation>
    </message>
    <message>
        <source>%1
Real Time Speed(m/s)</source>
        <translation type="unfinished">%1
即時速度(米/秒)</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation>上傳</translation>
    </message>
    <message>
        <source>Upload to server ......</source>
        <translation>上傳到伺服器 ……</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>繼續</translation>
    </message>
    <message>
        <source>Upload succeed</source>
        <translation>上傳成功</translation>
    </message>
    <message>
        <source>Upload fail</source>
        <translation>上傳失敗</translation>
    </message>
</context>
<context>
    <name>CLbsTrack</name>
    <message>
        <source>Track</source>
        <translation type="unfinished">跟蹤</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>開始</translation>
    </message>
    <message>
        <source>Stop</source>
        <translation>停止</translation>
    </message>
</context>
<context>
    <name>CPluginAppMotion</name>
    <message>
        <source>Motion</source>
        <translation>運動</translation>
    </message>
    <message>
        <source>Lbs</source>
        <translation>定位</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Application Infomation</source>
        <translation type="vanished">應用資訊</translation>
    </message>
    <message>
        <source>Application:</source>
        <translation type="vanished">應用:</translation>
    </message>
</context>
</TS>
